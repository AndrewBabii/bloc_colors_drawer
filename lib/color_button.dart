import 'package:flutter/material.dart';

class ColorButton extends StatelessWidget {
  ColorButton({this.color, this.changeColor});

  final Color color;
  final Function changeColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: changeColor,
      child: Container(
        color: color,
        height: 50,
        width: 80,
      ),
    );
  }
}
