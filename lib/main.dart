import 'package:block_practice_colors_drawer/bloc/color_bloc.dart';
import 'file:///C:/Users/AppVesto/AndroidStudioProjects/block_practice_colors_drawer/lib/color_button.dart';
import 'package:block_practice_colors_drawer/bloc/color_event.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (BuildContext ctx) => ColorBloc(),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ColorBloc _colorBloc = BlocProvider.of<ColorBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('bloc'),
      ),
      body: Center(
        child: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: StreamBuilder(
            stream: _colorBloc,
            builder: (context, snapshot) {
              return ColoredBox(
                color: _colorBloc.state,
              );
            },
          ),
        ),
      ),
      drawer: Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ColorButton(
              color: Colors.green,
              changeColor: () => _colorBloc.add(ColorEvent.setGreenEvent),
            ),
            ColorButton(
              color: Colors.blue,
              changeColor: () => _colorBloc.add(ColorEvent.setBlueEvent),
            ),
            InkWell(
              onTap: () => _colorBloc.add(ColorEvent.setRedEvent),
              child: ColorButton(
                color: Colors.red,
                changeColor: () => _colorBloc.add(ColorEvent.setRedEvent),
              ),
            ),
          ],
        ),
      ),
      // backgroundColor: _colorBloc.state,
    );
  }
}
