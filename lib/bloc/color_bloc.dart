import 'package:block_practice_colors_drawer/bloc/color_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ColorBloc extends Bloc<ColorEvent, Color>{

  Color _color /*= Colors.yellow*/;

  @override
  Stream<Color> mapEventToState(ColorEvent event) async*{
    switch(event){
      case ColorEvent.setBlueEvent:
        print('mapEventToState set bl');
        _color = Colors.blue;
        break;
      case ColorEvent.setGreenEvent:
        print('mapEventToState set gr');
        _color = Colors.green;
        break;
      case ColorEvent.setRedEvent:
        print('mapEventToState set red');
        _color = Colors.red;
        break;
    }
    yield _color;
  }

  ColorBloc(): super(Colors.yellow);
}