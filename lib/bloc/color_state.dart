import 'package:flutter/material.dart';

abstract class ColorState{}


class ColorUnsetState extends ColorState{}

class ColorSetState extends ColorState{
  final Color color;

  ColorSetState({this.color}): assert(color != null);
}